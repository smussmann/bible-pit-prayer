package main

//   Copyright 2020 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/smussmann/ordo"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	config := ordo.Config{
		ServiceTemplate: "./main.template.html",
		PageTemplates: map[string]string{
			"/":          "./index.template.html",
			"/copyright": "./copyright.template.html",
		},
		ESVToken: esvToken,
	}
	http.Handle("/", ordo.GetServeMux(config))
	log.Printf("Serving at :%s", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
