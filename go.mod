module bitbucket.org/smussmann/bible-pit-prayer

go 1.14

require (
	bitbucket.org/smussmann/ordo v0.0.0-20200311002730-d2998835490b
	github.com/vjeantet/eastertime v0.0.0-20160228130558-3941bbcf8209 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
)
